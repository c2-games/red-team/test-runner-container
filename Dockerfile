FROM rockylinux:9.3-minimal

RUN microdnf upgrade -y
RUN microdnf install -y yum-utils
RUN yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
RUN microdnf install -y python3 python3-pip python3-jmespath ansible-core terraform git \
    findutils
RUN microdnf clean all -y
RUN ansible-galaxy collection install community.general
