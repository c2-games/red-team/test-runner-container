# Test Runner Container

A simple image used for executing Red Team automated tests using GitLab CI.

## Key features

- Based on RockyLinux 9.3 for binary compatibility with RHEL 9.3
- Python 3
- Ansible
- Terraform
